---
layout: page
title: Eventos
permalink: /eventos/
---

Aqui estão eventos que já fizemos junto a comunidade Debian.

<br>

{% for event in site.data.events %}
---
<div id="{{ event.name | replace: ' ', '-' }}">
    {{ contrib.name | replace: ' ', '-' }}
    <h3><a href="#{{ event.name | replace: ' ', '-' }}">{{ event.name }}</a></h3>
    <p>{{ event.description }}</p>
    {{ event.link }}
</div>
{% endfor %}
