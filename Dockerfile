FROM jekyll/jekyll:4.2.2

ENV JEKYLL_ENV production
ENV LC_ALL C.UTF-8

WORKDIR /site

ADD Gemfile .
ADD Gemfile.lock .

RUN bundle install

COPY . .

EXPOSE 4000

ENTRYPOINT bundle exec jekyll server -H 0.0.0.0