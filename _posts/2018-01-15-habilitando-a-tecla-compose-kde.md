---
layout: post
title:  Habilitando a tecla COMPOSE KDE
date:   2018-01-15 00:00:00 -0300
description: Vamos criar um pendrive bootável utilizando o comando dd através do terminal. Antes é preciso entender a sintaxe do comando dd.
categories: DICAS KDE COMPOSE-KEY
author:  Marcio De Souza Oliveira
icon: laptop
color: default
---

Olá a todos,

Hoje vamos dar uma dica rápida para quem tem notebooks/computadores com o teclado americano e não quer ficar mudando toda hora o mapa do teclado para digitar os acentos e cedilhas do nosso bom e velho PT-BR.

1. Compose key

Segundo a wiki do KDE[1], a tecla compose é usada para inserir caracteres usando um teclado que não dispõe normalmente de algumas teclas específicas. O teclado americano por exemplo não possuí o cedilha e se você quiser utilizá-lo sem alterar o layout do teclado, você terá que habilitar a Compose key do KDE, isso para quem usa KDE 🙂 , para utilizar acentos gráficos, por exemplo áéíóú, ela também será necessária.

2. Habilitando a compose key do KDE

É bem fácil habilitar a tecla de composição no KDE para isso abra  “Configurações do Sistema > Dispositivos de Entrada > Teclado > Avançado”.


Configurações do sistema > Dispositivos de Entrada

Disp. de entrada > Teclado
Em Avançado marque o checkbox “Configurar as opções do teclado” e depois marque o checkbox “Posição da tecla de composição” e por último escolha qual será sua tecla de composição, no meu caso marquei o checkbox do “Alt direito” porque a escolhi.


Habilitando o Alt direito como tecla de composição
Pronto!!! Feito isso nunca mais você terá que ficar alterando seu layout de teclado entre “US” e “Português do Brasil” para poder digitar as palavras com acento.

Vamos lá, teste! Para digitar qualquer acento gráfico no layout americano, basta você pressionar a tecla de composição(Alt direito) + acento + vogal/consoante.  E o cedilha como é? Simples basta pressionar a tecla de composição(Alt direito) + acento + a tecla c.

Deixe um like na página da nossa comunidade!

