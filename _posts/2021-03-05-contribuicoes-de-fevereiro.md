---
layout: post
title:  "Contribuições do mês de Fevereiro"
date:   2021-03-05 00:00:00 -0300
description: Documentando as atividades do mês de fevereiro
categories: CONTRIBUICAO
author: Admin
color: default
---

No mês de janeiro, iniciamos a documentar as nossas atividades.
Desde então, a participação aumentou e falamos sobre vários tópicos
diferentes como autopkgtest, schroot, BTS e demais coisas relacionadas
à empacotamento. Quem tiver interesse em participar, nossos encontros
semanais são todas as quintas às 20 horas, no horário de Brasília.

Ao longo do mês foram realizadas 11 contribuições, com a participação
de 13 pessoas, ao decorrer dos 4 encontros.

Veja abaixo a lista de contribuições.

#### Arthur Diniz

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Pacote</th>
            <th>Versão</th>
            <th>Data</th>
            <th>Sponsor</th>
        </tr>
    </thead>
    <tbody> 
       <tr>
            <td class="text-center">1</td>
            <td><a href="https://tracker.debian.org/news/1228846/accepted-golang-github-alessio-shellescape-141-1-source-all-into-unstable-unstable/">golang-github-alessio-shellescape</a></td>
            <td>1.4.1-1</td>
            <td>2021-02-06</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">2</td>
            <td><a href="https://tracker.debian.org/news/1228847/accepted-golang-github-munnerz-goautoneg-00git20191010a7dc8b6-1-source-all-into-unstable-unstable/">	golang-github-munnerz-goautoneg</a></td>
            <td>0.0~git20191010.a7dc8b6-1</td>
            <td>2021-02-06</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">3</td>
            <td><a href="https://tracker.debian.org/news/1228848/accepted-golang-k8s-klog-250-1-source-all-into-unstable-unstable/">golang-k8s-klog</a></td>
            <td>2.5.0-1</td>
            <td>2021-02-06</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">4</td>
            <td><a href="https://tracker.debian.org/news/1228890/accepted-golang-github-alessio-shellescape-141-2-source-into-unstable/">golang-github-alessio-shellescaper</a></td>
            <td>1.4.1-2</td>
            <td>2021-02-07</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">5</td>
            <td><a href="https://tracker.debian.org/news/1228891/accepted-golang-github-munnerz-goautoneg-00git20191010a7dc8b6-2-source-into-unstable/">golang-github-munnerz-goautoneg</a></td>
            <td>0.0~git20191010.a7dc8b6-2</td>
            <td>2021-02-07</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">6</td>
            <td><a href="https://tracker.debian.org/news/1228892/accepted-golang-k8s-klog-250-2-source-into-unstable/">golang-k8s-klog</a></td>
            <td>2.5.0-2</td>
            <td>2021-02-07</td>
            <td>Sergio Durigan</td>
        </tr>
    </tbody>
</table>

#### J.Paulo Oliveira

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Pacote</th>
            <th>Versão</th>
            <th>Data</th>
            <th>Sponsor</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td><a href="https://tracker.debian.org/news/1227139/accepted-proxycheck-049a-6-source-into-unstable/">proxycheck [QA]</a></td>
            <td>0.49a-6</td>
            <td>2021-02-01</td>
            <td>Adam Borowski</td>
        </tr>
        <tr>
            <td class="text-center">2</td>
            <td><a href="https://tracker.debian.org/news/1228434/accepted-iptotal-033-14-source-into-unstable/">iptotal [QA]</a></td>
            <td>iptotal [QA]</td>
            <td>0.3.3-14</td>
            <td>Adam Borowski</td>
        </tr>
        <tr>
            <td class="text-center">3</td>
            <td><a href="https://tracker.debian.org/news/1232866/accepted-golang-github-cli-browser-100-1-source-amd64-into-experimental-experimental/
            ">golang-github-cli-browser</a></td>
            <td>iptotal [QA]</td>
            <td>1.0.0-1</td>
            <td>Samuel Henrique</td>
        </tr>
    </tbody>
</table>

#### Sergio Cipriano

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Pacote</th>
            <th>Versão</th>
            <th>Data</th>
            <th>Sponsor</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td><a href="https://tracker.debian.org/news/1234018/accepted-typer-032-1exp1-source-all-into-experimental-experimental/">python3-typer</a></td>
            <td>0.3.2-1~exp1</td>
            <td>2021-02-27</td>
            <td>Sergio Durigan</td>
        </tr>
    </tbody>
</table>
<br/>
<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Salsa</th>
            <th>MR</th>
            <th>Data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td><a href="https://salsa.debian.org/ci-team/debci">ci-team/debci</a></td>
            <td><a href="https://salsa.debian.org/ci-team/debci/-/merge_requests/143">Fix Style/GlovalVars issue</a></td>
            <td>2021-02-22</td>
        </tr>
    </tbody>
</table>

<p style="margin-top:550px"></p>
