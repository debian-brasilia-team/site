---
layout: post
title:  "Montando Imagens VDI/VMDK/QCOW2"
date:   2018-01-09 00:00:00 -0300
description: Olá a todos, Hoje vamos abordar um assunto que certamente será útil para quem utiliza máquinas virtuais. 
categories: IMAGES VDI VMDK QCOW2
author:  Marcio de Souza Oliveira
icon: terminal
color: default
---

Olá a todos,

Hoje vamos abordar um assunto que certamente será útil para quem utiliza máquinas virtuais. A ideia aqui é mostrar como você pode “montar” as partições que estão dentro de um arquivo VDI(Virtualbox Disk Image), VMDK(Vmware Disk Image), ou QCOW2(Formato de imagem de disco utilizado pelo QEMU). Na prática você vai conseguir as partições de sua máquina virtual a partir de sua máquina física.

Então mãos a obra! 🙂

1. Instalando as dependências
Vamos utilizar o utilitário qemu-nbd  para fazer o mapeamento do disco virtual(VDI, QCOW2, VMDK) para um dispositivo nbd no sistema. Então vamos a sua instalação:
```bash
apt-get update && apt-get install qemu-utils
```

2. Carregando o módulo nbd
Para carregar o módulo do NBD(Network Block Device) vamos usar o comando modprobe:
```bash
modprobe nbd
```

3. Mapeando o disco virtual para o dispositivo nbd
Agora vamos conectar o disco virtual(VDI, VMDK, QCOW2) ao dispositivo NBD(nbd0). No meu caso vou conectar o disco virtual “Debian SID.vdi”:
```bash
qemu-nbd -c /dev/nbd0 /home/marcio/Debian\ Sid.vdi
```
Dependendo do kernel que você estiver utilizando, ele automaticamente detectará as partições. Você pode checar isso utilizando o comando abaixo:
```bash
fdisk -l /dev/nbd0
```
Partições do arquivo Debian Sid.vdi
Se as partições da sua máquina virtual não aparecerem, você pode executar o comando abaixo para o kernel fazer o mapeamento:
```bash
kpartx -a /dev/nbd0
```
Pronto agora é só montar os dispositivo nbd0p1, no meu caso, que eu terei acesso ao sistema de arquivos que está na primeira partição do VDI. Para VMDK e QCOW2 é o mesmo procedimento basta apontar para o arquivo de imagem de disco desejado.

4. Desconectando o disco virtual
Depois da utilização você deve desconectar o disco virtual para isso execute:
```bash
kpartx -d /dev/nbd0 && qemu-nbd -d /dev/ndb0
```