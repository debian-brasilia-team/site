---
layout: post
title:  "Mulheres Brasil"
date:   2018-01-16 00:00:00 -0300
description: Meninas/Garotas/Mulheres de todas as idades, este é um convite especial para todas vocês!
categories: Brasil Mulheres
author: Admin
icon: female
color: default
---

#### Mulheres brasileiras, venham participar do Projeto Debian!

Meninas/Garotas/Mulheres de todas as idades, este é um convite especial para todas vocês!

Que tal colaborar voluntariamente com o time brasileiro do maior projeto de Software Livre do planeta? Venham aprender a traduzir, empacotar, organizar eventos e muito mais


Quer saber mais… https://wiki.debian.org/MulheresBrasil