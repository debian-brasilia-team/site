---
layout: post
title:  "Aprendendo o básico sobre ACL's no linux"
date:   2018-01-09 00:00:00 -0300
description: Vamos utilizar ACL’s aqui para gerenciar permissões no Linux de forma prática.
categories: ACL ACLS ACLS-LINUX
author:  Gilmar Dos Reis
icon: terminal
color: default
---

Vamos utilizar ACL’s aqui para gerenciar permissões no Linux de forma prática. Primeiramente é preciso verificar se o kernel que está sendo utilizado é compatível com ACL’s. Para isso iremos utilizar os seguintes comandos:

```bash
root@debian:~# grep -i acl /boot/config-$(uname -r)
```

No comando acima temos um GREP listando tudo sobre acl dentro do arquivo config-Seu-Kernel, pois o comando uname -r irá retornar a versão do kernel do sistema. Observa-se que o recurso de ACL’s está disponível no kernel, porém isso não basta, precisamos agora instalar as ferramentas de administração de ACL no debian ou em outras distros que você queira.

#### Instalando ferramentas de administração de ACL

```bash
root@debian:~# apt-get update
root@debian:~# apt-get install acl
```

#### Habilitando ACL na partição que vai ser utilizada

Agora é preciso habilitar o recurso de ACL na partição que se deseja utilizar. Edite o arquivo /etc/fstab, adicione a string “acl” nas opções do sistema de arquivo desejado:
```bash
root@debian:~# vim /etc/fstab
```
---

#### BLKID para facilitar a identificação do disco:

Se você ficar na dúvida sobre a partição que vai ativar ACL, veja que o debian monta as partições pelo UUID. Se estiver em dúvida sobre os UUID’s de sua partições basta utilizar um dos comando abaixo:
```bash
root@debian:~# blkid

root@debian:~# blkid | grep -i sda1

root@debian:~# blkid /dev/sda1

root@debian:~# ls -alh /dev/disk/by-uuid/*
```

É importante observar que se o sistema de arquivos (partição) já está montado, deve-se reinciar o sistema ou simplesmente remontar a partição:
```bash
root@debian:~# mount / -o remount,acl
```

Se não ouve nenhum erro durante os processos anteriores então estamos prontos para iniciar com a implementação de ACL’s

---

#### Entendendo e Configurando ACL
Imagine agora que você tenha um diretório e quer que dois grupos diferentes tenham permissões também diferentes sobre esse diretório, ou mesmo personalizar dois usuários para permissões diferentes. Vamos trabalhar essa situação.

Utilize o ROOT  para criar uma pasta no diretório /home de sua máquina e defina as permissões como 700, assim somente o ROOT tem acesso a esse diretório:
```bash
root@debian:~# mkdir cebolinha

root@debian:~#  chmod 700 cebolinha
```


Crie dois usuários como especificado abaixo:
```bash
root@debian:~# adduser kiko

root@debian:~# adduser chaves
```
> Obs: Quando criamos os usuário “kiko” e “chaves” um grupo “kiko” e um grupo “chaves” foi criado.


Agora vamos criar um grupo com o nome “madruga” e inserir o usuário “chaves” no grupo madruga:
```bash
root@debian:~# addgroup madruga

root@debian:~# addgroup chaves madruga
```

Observe que o diretório “cebolinha” que criamos em /home/cebolinha está com permissõe apenas para o ROOT. Porem através de ACLs nós iremos dar permissões para que o usuário kiko possa acessar com permissões de “LEITURA E EXECUÇÃO” e para os membros do grupo “madruga” daremos permissões de “LEIURA E ESCRITA”. Mãos a Obra:


Primeiramente vamos tentar acessar o diretório /home/cebolinha com o usuário kiko e com o usuário chaves
```bash
root@debian:~# su kiko

root@debian:~# cd /home/cebolinha
```

Você deverá receber uma mensagem que não tem permissão
```bash
root@debian:~# exit

root@debian:~#  su chaves

root@debian:~# cd /home/cebolinha
```

Você deverá receber uma mensagem que não tem permissão
```bash
root@debian:~# exit
```

Vamos então aplicar uma ACL simples para que o usuário “KIKO” possa ter Leitura e Execução no diretório cebolinha :
```bash
root@debian:~# setfacl -m u:kiko:rx /home/cebolinha/

root@debian:~# su kiko

root@debian:~# cd /home/cebolinha
```

Veja que agora você pode ler o que tem dentro do diretório cebolinha (tente um ls -lha, tente criar um arquivo e verá que não tem permissão para isso)


Vamos então aplicar uma ACL simples para que os membros do grupo “MADRUGA” possam ter Leitura Escrita e Execução no diretório cebolinha:
```bash
root@debian:~# setfacl -m g:madruga:rwx /home/cebolinha/

root@debian:~# su chaves

root@debian:~# cd /home/cebolinha
```

Veja que agora você pode ler o que tem dentro do diretório cebolinha (tente um arquivo e depois ls -lha)

---

### IMPORTANTE SABER

Se você executar um ls no /home você verá que agora o diretório “CEBOLINHA” está marcado com um sinal de “+” no campo de permissões, isso acontece porque o diretório “CEBOLINHA” está com ACL configurada. Se você que ver detalhes das pemissõe especiais atribuídas ao diretório que está com ACL configurada basta utilizar os comandos abaixo:

```bash
root@debian:~# getfacl /home/cebolinha/
```

Observem que ACL é um mundo dentro do linux, mas a intenção aqui é apenas ajudar no entendimento inicial. Lembrando que aqui não falei da remoção de ACLs, mas em breve teremos esse complemento