---
layout: post
title:  Abra um arquivo com o aplicativo padrão a partir do terminal
date:   2018-02-23 00:00:00 -0300
description: Se mesmo quando você está usando seu ambiente desktop preferido (GNOME, KDE, etc), você odeia quando tem que abandonar o conforto do seu emulador de terminal...
categories: DICAS ABRIR APLICATIVO ARQUIVO SHELL TERMINAL
author: David Polverari
icon: laptop
color: default
---

Se mesmo quando você está usando seu ambiente desktop preferido (GNOME, KDE, etc), você odeia quando tem que abandonar o conforto do seu emulador de terminal para abrir um arquivo PDF, tocar um MP3 ou editar um arquivo ODT? Seus problemas acabaram: o xdg-open, parte da suíte de aplicativos xdg-utils, do projeto FreeDesktop, permite abrir qualquer arquivo com seu aplicativo padrão (conforme definido pela distribuição ou pelo usuário) a partir do shell. Deste modo, não é necessário lembrar se você tem que utilizar o evince para abrir um PDF, nem ir ao gerenciador de arquivos e clicar no arquivo, como mostra o exemplo abaixo:
```bash
$ xdg-open documento.pdf
```

Espero que aproveitem a dica, e até a próxima!