# Development

## Dependencies
    - Python 3
    - apt install libpq-dev gcc python3-dev musl-dev

## Python Virtual Environment

```bash
# Create environment
python3 -m venv venv

# To activate the environment
source venv/bin/activate

# When you finish you can exit typing
deactivate
```

## Install dependencies

```bash
pip3 install -r requirements.txt
```

## Run

```bash
python3 main.py
```