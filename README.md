## Getting Started

Remember you need to wait for your site to build before you will be able to see your changes.  You can track the build on the **Pipelines** tab.

#### Dependencies

- Docker
- Python3

#### Build

```
make build
```

#### Run

```
make run
```

#### Shell

```
make shell
```


#### Update contributors page

```shell
make update-contributors
```

## Manualy

To work locally with this project, you'll have to follow the steps below:

1. [Install](https://jekyllrb.com/docs/installation/) Jekyll
2. Download dependencies: `bundle`
3. Build and preview: `bundle exec jekyll serve`
4. Add content

The above commands should be executed from the root directory of this project.

Read more at Jekyll's [documentation](https://jekyllrb.com/docs/).