build:
	docker build -t debian-brasilia-site --network host .

run:
	docker run -it -v $(PWD):/site --network host debian-brasilia-site

shell:
	docker run -it -v $(PWD):/site --network host --entrypoint bash debian-brasilia-site

update-contributors:
	docker build -t debian-brasilia-contributors --network host -f ./contributors/Dockerfile .
	docker run -it --network host -v $(PWD):/site debian-brasilia-contributors