---
layout: page
title: Contribuições
permalink: /contribuicoes/
---

Aqui estão as nossas contribuições para a comunidade Debian.


{% assign uploads = site.data.uploads["uploads"] %}

---

<h4>Uploads</h4>

<ul>
  <li>Unstable: {{ uploads.unstable }}</li>
  <li>Experimental: {{ uploads.experimental }}</li>
</ul>

---

<div>
{% for contrib_hash in site.data.contrib %}
    {% assign contrib = contrib_hash[1] %}
    <details>
    <summary style="font-size: 20px;"><a id="{{ contrib.name | replace: ' ', '-' }}" href="#{{ contrib.name | replace: ' ', '-' }}">{{ contrib.name }}</a><ul>{% if contrib.count.unstable %} <li>unstable: {{contrib.count.unstable}}</li> {% endif %} {% if contrib.count.experimental %}<li> experimental: {{contrib.count.experimental}}</li> {% endif %}</ul></summary>
    <br>
    <table class="table">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Pacote</th>
                <th>Versão</th>
                <th>Data</th>
                <th>Distribution</th>
                <th>Sponsor</th>
            </tr>
        </thead>
        <tbody>
        {% for upload in contrib.uploads %}
            <tr>
                <td class="text-center">{{ forloop.index }}</td>
                <td>
                    <a href="{{ upload.tracker }}">{{ upload.package }}</a>
                </td>
                <td>{{ upload.version }}</td>
                <td>{{ upload.date }}</td>
                <td>{{ upload.distribution }}</td>
                <td>{{ upload.sponsor }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    </details>

{% endfor %}
</div>


<p style="margin-top:400px"></p>
